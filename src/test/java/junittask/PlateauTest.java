package junittask;

import junittask.plateau.Plateau;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
//
public class PlateauTest {
    private static Plateau plateau;
    private static org.apache.logging.log4j.Logger LOG;

    @BeforeAll
    static void initAll() {
        LOG = LogManager.getLogger(PlateauTest.class);
    }

    @BeforeEach
    void init() {
        plateau = new Plateau();
        LOG.info("BeforeEach running.");
    }

    @DisplayName("First test")
    @Test
    void testMethod1() {
        LOG.info("First test running.");
        plateau.setArray(1, 1, 1, 1, 1, 2, 2, 3, 3, 4, 5, 723, 7, 2, 4);
        assertEquals("0 5", plateau.getSequence());
        LOG.info("successfully");
    }

    @DisplayName("Second test")
    @Test
    void testMethod2() {
        LOG.info("Second test running.");
        plateau.setArray(2,2,2,3,3,3,1,1,1,4,4,4,5,5,5,5);
        assertFalse(plateau.getSequence().isEmpty());
        LOG.info("successfully");
    }

    @AfterEach
    void tearDown() {
        LOG.info("After each running.");
    }

    @AfterAll
    static void tearDownAll() {
        LOG.info("AfterAll test running.");
        plateau = null;
        LOG = null;
    }
}
